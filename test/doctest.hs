-- SPDX-License-Identifier: MIT
-- SPDX-FileCopyrightText: 2023 Jan Tojnar <jtojnar@gmail.com>

module Main where

import Test.DocTest

main :: IO ()
main = doctest [
	"-XLambdaCase",
	"-XOverloadedStrings",
	"-fno-warn-tabs",
	"-interactive-print=Text.Show.Unicode.uprint",
	"lib/Hroch/Features/CleanUrls.hs",
	"lib/Hroch/Internal/Utils.hs"
	]
