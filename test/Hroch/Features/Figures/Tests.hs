-- SPDX-License-Identifier: MIT
-- SPDX-FileCopyrightText: 2023 Jan Tojnar <jtojnar@gmail.com>

{-# LANGUAGE OverloadedStrings #-}

module Hroch.Features.Figures.Tests (tests) where

import Data.Text (Text)
import Hroch.Features.Figures (implicitFigures, implicitFiguresWith, bs4GalleryStyle)
import Text.Pandoc
import Test.Tasty (TestTree, testGroup)
import Test.Tasty.HUnit ((@=?), testCase)

tests :: TestTree
tests =
	testGroup "Hroch.Features.Figures.Tests"
	[
	implicitFigureWithoutCaptionTest,
	implicitFigureWithCaptionTest,
	implicitFigureBs4WithoutCaptionTest,
	implicitFigureBs4WithCaptionTest
	]

runPandocFilter :: (Pandoc -> Pandoc) -> Text -> IO Text
runPandocFilter pandocFilter source = runIOorExplode $ readHtml def source >>= return . pandocFilter >>= writeHtml5String def

implicitFigureWithoutCaptionTest :: TestTree
implicitFigureWithoutCaptionTest = testCase "implicitFigureWithoutCaption" $ do
	actual <- runPandocFilter implicitFigures "<p><img src=\"foo.jpg\"></p>"
	expected @=? actual
	where
		expected =
			"<div class=\"card card-sm\">\n\
			\<img src=\"foo.jpg\" class=\"card-img\" />\n\
			\</div>"

implicitFigureWithCaptionTest :: TestTree
implicitFigureWithCaptionTest = testCase "implicitFigureWithCaption" $ do
	actual <- runPandocFilter implicitFigures "<p><img src=\"foo.jpg\" alt=\"Caption\"></p>"
	expected @=? actual
	where
		expected =
			"<div class=\"card card-sm\">\n\
			\<img src=\"foo.jpg\" class=\"card-img-top\" alt=\"Caption\" />\n\
			\<div class=\"card-block\">\n\
			\<div class=\"card-text\">\n\
			\Caption\n\
			\</div>\n\
			\</div>\n\
			\</div>"

implicitFigureBs4WithoutCaptionTest :: TestTree
implicitFigureBs4WithoutCaptionTest = testCase "implicitFigureBs4WithoutCaption" $ do
	actual <- runPandocFilter (implicitFiguresWith bs4GalleryStyle) "<p><img src=\"foo.jpg\"></p>"
	expected @=? actual
	where
		expected =
			"<div class=\"card card-sm\">\n\
			\<img src=\"foo.jpg\" class=\"card-img\" />\n\
			\</div>"

implicitFigureBs4WithCaptionTest :: TestTree
implicitFigureBs4WithCaptionTest = testCase "implicitFigureBs4WithCaption" $ do
	actual <- runPandocFilter (implicitFiguresWith bs4GalleryStyle) "<p><img src=\"foo.jpg\" alt=\"Caption\"></p>"
	expected @=? actual
	where
		expected =
			"<div class=\"card card-sm\">\n\
			\<img src=\"foo.jpg\" class=\"card-img-top\" alt=\"Caption\" />\n\
			\<div class=\"card-body\">\n\
			\<div class=\"card-text\">\n\
			\Caption\n\
			\</div>\n\
			\</div>\n\
			\</div>"
