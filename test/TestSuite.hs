-- SPDX-License-Identifier: MIT
-- SPDX-FileCopyrightText: 2023 Jan Tojnar <jtojnar@gmail.com>

module Main (
	main
) where

import Test.Tasty (defaultMain, testGroup)

import qualified Hroch.Features.Figures.Tests

main :: IO ()
main =
	defaultMain $ testGroup "hroch"
	[
	Hroch.Features.Figures.Tests.tests
	]
