---
title: Gallery
---

<div class="figuregroup">
![Baby hippo](baby-hippo.jpg){.thumb-sm data-lightbox="o1"}
![Hippo enjoying a melon](hippo-melon.jpg){.thumb-sm data-lightbox="o1"}
</div>

Photos by [Petar Ubiparip](https://pixabay.com/users/fotos1992-6533637/).

