cabal-version: 3.8

name: hroch
version: 0.3.0
synopsis: Opinionated static site generator
category: Web
author: Jan Tojnar
maintainer: jtojnar@gmail.com
license: MIT
license-file: LICENSE

extra-source-files:
  CHANGELOG.md

flag gd
  description: Provide gd thumbnailer (requires gd and hsexif)


common base
  default-language: Haskell2010
  ghc-options:
    -Wall
    -fno-warn-tabs

common test
  import: base
  hs-source-dirs:
    test
  build-depends:
    base,
    hroch,

common haddock-example-test
  import: test
  hs-source-dirs:
    lib/Hroch/Features
  build-depends:
    hakyll,
  ghc-options:
    -F -pgmF extract-example
  build-tool-depends:
    hroch:extract-example,

library
  import: base
  hs-source-dirs:
    lib
  build-depends:
    base >= 4.8 && < 4.19,
    bytestring >= 0.11 && < 0.13,
    filepath >= 1.4 && < 1.5,
    hakyll >= 4.8 && < 4.17,
    http-types >= 0.12 && < 0.13,
    mtl >= 2.2 && < 2.4,
    modern-uri >= 0.3 && < 0.4,
    directory >= 1.2 && < 1.4,
    pandoc >= 1.17 && < 3.2,
    pandoc-types >= 1.20 && < 1.24,
    text,
    wai >= 3.2 && < 3.3,
    wai-app-static >= 3.1 && < 3.2,
  exposed-modules:
    Hroch.Features.CleanUrls
    Hroch.Features.ExternalMetadata
    Hroch.Features.Figures
    Hroch.Features.Gallery
    Hroch.Features.LinkResolver
    Hroch.Features.Menu
    Hroch.Features.Thumbnail
    Hroch.Features.WatchTransformer
    Hroch.Internal.Utils

  if flag(gd)
    build-depends:
      gd >= 3000.7 && < 3000.8,
      hsexif >= 0.6 && < 0.7,
    exposed-modules:
      Hroch.Features.Thumbnail.Gd

executable hroch
  import: base
  main-is: Main.hs
  hs-source-dirs:
    src
  build-depends:
    base,
    containers >= 0.5 && < 0.7,
    directory,
    extra >= 1.7 && < 1.8,
    filepath,
    hakyll,
    hroch,
    pandoc,
    process >= 1.6 && < 1.7,
    time >= 1.9 && < 1.13,
  ghc-options:
    -- Required for watch on Windows.
    -threaded

executable extract-example
  import: base
  build-depends:
    base,
    regex-applicative >= 0.3 && < 0.4,
  hs-source-dirs:
    test
  main-is: extract-example.hs

test-suite example-clean-urls
  import: haddock-example-test
  main-is: CleanUrls.hs

test-suite example-external-metadata
  import: haddock-example-test
  main-is: ExternalMetadata.hs

test-suite example-gallery
  import: haddock-example-test
  main-is: Gallery.hs
  build-depends:
    pandoc,

test-suite example-figures
  import: haddock-example-test
  main-is: Figures.hs
  build-depends:
    pandoc,

test-suite example-link-resolver
  import: haddock-example-test
  main-is: LinkResolver.hs
  build-depends:
    filepath,

test-suite example-menu
  import: haddock-example-test
  main-is: Menu.hs

test-suite example-thumb
  import: haddock-example-test
  main-is: Thumbnail.hs
  if !flag(gd)
    buildable: False

test-suite example-thumb-custom
  import: haddock-example-test
  main-is: Thumbnail.hs
  build-depends:
    directory,
    filepath,
    process,
  ghc-options:
    -optF "In addition to the provided GD-based thumbnailer, you can use a custom one:"

test-suite example-watch-transformer
  import: haddock-example-test
  main-is: WatchTransformer.hs
  build-depends:
    bytestring,
    stringsearch >= 0.3 && < 0.4,
    wai-app-static,


test-suite doctest
  import: test
  main-is: doctest.hs
  build-depends:
    doctest >= 0.8,
    unicode-show >= 0.1 && < 0.2,


test-suite tests
  import: test
  main-is: TestSuite.hs
  ghc-options:
    -threaded

  other-modules:
    Hroch.Features.Figures.Tests

  build-Depends:
    tasty >= 0.11 && < 1.5,
    tasty-hunit >= 0.9 && < 0.11,
    text,
    pandoc,
