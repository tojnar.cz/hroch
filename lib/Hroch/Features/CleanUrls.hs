-- SPDX-License-Identifier: MIT
-- SPDX-FileCopyrightText: 2023 Jan Tojnar <jtojnar@gmail.com>

{-|
= Clean URLs
This is a pair of helpers facilitating [clean URLs](https://en.wikipedia.org/wiki/Clean_URL):

- 'cleanRoute' is a 'Routes' object that creates @index.html@ file in a directory named after the (Markdown) file.
- 'cleanUrlField' is a 'Context' including a @url@ field containing the route with filename removed when it is @index.html@. Useful for feeds and post listings.

== Usage

Set route and append the field to relevant contexts (e.g. for feeds or directory listings):

@
{-# LANGUAGE OverloadedStrings #-}
import Hakyll
import Hroch.Features.CleanUrls (cleanRoute, cleanUrlField)
import Hroch.Internal.Utils (runHakyllWithHelpFlagForTests)

main :: IO ()
main = runHakyllWithHelpFlagForTests $ hakyll $ do
	match "**.md" $ do
		route $ cleanRoute

		compile $ do
			pandocCompiler
				>>= saveSnapshot contentSnapshot
				>>= loadAndApplyTemplate "templates/layout.html" pageContext

	create ["feed.atom"] $ do
		route idRoute
		compile $ do
			let feedCtx = pageContext <> bodyField "description"

			pages <- fmap (take 10) . recentFirst =<< loadAllSnapshots "**.md" contentSnapshot
			renderAtom feedConfiguration feedCtx pages

pageContext :: Context String
pageContext = cleanUrlField <> defaultContext

contentSnapshot :: Snapshot
contentSnapshot = "content"

feedConfiguration :: FeedConfiguration
feedConfiguration =
	FeedConfiguration {
		feedTitle = "Example Site",
		feedDescription = "Latest news about Example science.",
		feedAuthorName = "",
		feedAuthorEmail = "",
		feedRoot = "https://example.org"
	}
@
-}
module Hroch.Features.CleanUrls (
	cleanPath,
	uncleanPath,
	cleanRoute,
	cleanUrlField,
) where

import Data.Maybe (fromMaybe)
import Hakyll (Context, Routes, customRoute, field, getRoute, itemIdentifier, toFilePath, toUrl)
import System.FilePath ((</>), dropExtension, dropFileName, replaceExtension, takeBaseName, takeDirectory, takeFileName)

{-|
Replaces path to a file by a path to “index.html” file in a directory named after the file.
For files with “index” as a base name, the extension will be changed to “html”.

>>> cleanPath "foo/bar.md"
"foo/bar/index.html"

>>> cleanPath "foo/bar/index.md"
"foo/bar/index.html"
-}
cleanPath :: FilePath -> FilePath
cleanPath path =
	if takeBaseName path == "index" then
		replaceExtension path "html"
	else
		dropExtension path </> "index.html"

{-|
Inverse of 'cleanPath'.
Since the former is not injective, we need to return both options.

>>> uncleanPath "foo/bar/index.html"
("foo/bar.md","foo/bar/index.md")
-}
uncleanPath :: FilePath -> (FilePath, FilePath)
uncleanPath path =
	let
		dir = takeDirectory path
	in
		(dir <> ".md", dir </> "index.md")


-- | Route based on 'cleanPath'.
cleanRoute :: Routes
cleanRoute = customRoute (cleanPath . toFilePath)

-- | Field providing a nice URL for an item with a “clean route”.
-- (It points to a directory rather than “index.html” file within.)
--
-- Uses “url” key, which is recognized by 'Hakyll.renderAtom' & co.
-- It is also useful for article listings.
cleanUrlField :: Context a
cleanUrlField = field "url" $ \item -> do
	let identifier = itemIdentifier item
	route <- fromMaybe (fail ("Trying to access url field for a page with missing route: " ++ toFilePath identifier)) <$> (getRoute identifier)
	let target =
		if takeFileName route == "index.html" then
			dropFileName route
		else
			route
	return (toUrl target)
