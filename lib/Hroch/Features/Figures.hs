-- SPDX-License-Identifier: MIT
-- SPDX-FileCopyrightText: 2023 Jan Tojnar <jtojnar@gmail.com>

{-# LANGUAGE OverloadedStrings #-}

{-|
= Custom implicit figures

When [@implicit_figures@ extension](https://pandoc.org/MANUAL.html#extension-implicit_figures) is enabled, Pandoc’s /Markdowns reader/ will convert @Image@ AST nodes contained in a 'Para' into figures. Those would then eventually be turned into @\<figure\>@ elements by Pandoc’s /HTML writer/. But since filters transform the output of a reader, the extension would not be able to process the images linkified by the 'Hroch.Features.Thumbnail.makeThumbnails' filter. To be still able to have thumbnails turned into figures, we are replicating the functionality of the extension using a filter.

Additionally, this allows us to customize the format of the produced figure. It defaults o 'bs4AlphaGalleryStyle', which uses [Bootstrap 4 image cards](https://getbootstrap.com/docs/4.0/components/card/) instead of plain @\<figure\>@ and @\<figcaption\>@ elements.

== Usage

Simply add 'implicitFigures' to filters. If 'Hroch.Features.Thumbnail.makeThumbnails' is used, it needs to run before it, and the thumbnail will be used for the 'Image'. If 'Hroch.Features.Gallery.figureGroups' is used, it should run after it. It is best to disable the native @implicit_figures@ extension to avoid double wrapping images without a caption.

@
{-# LANGUAGE OverloadedStrings #-}
import Hakyll
import Hroch.Features.Figures (implicitFigures)
import Hroch.Internal.Utils (runHakyllWithHelpFlagForTests)
import Text.Pandoc.Extensions (Extension (Ext_implicit_figures), disableExtension)
import Text.Pandoc.Options (readerExtensions)

markdownCompiler :: Compiler (Item String)
markdownCompiler = pandocCompilerWithTransform readOpts writeOpts filters
	where
		readOpts = defaultHakyllReaderOptions { readerExtensions = disableExtension Ext_implicit_figures (readerExtensions defaultHakyllReaderOptions) }
		writeOpts = defaultHakyllWriterOptions
		filters = implicitFigures

main :: IO ()
main = runHakyllWithHelpFlagForTests $ hakyll $ do
	match "**.md" $ do
		compile markdownCompiler
@

== Notes

* 'implicitFigures' uses @card-block@ class from an alpha version of Bootstrap 4. Use @implicitFigures bs4GalleryStyle@ or @implicitFigures bs5GalleryStyle@ if your site uses a different version of Bootstrap. Or you can pass a custom 'GalleryStyle'.
-}
module Hroch.Features.Figures (
	GalleryStyle (..),
	bs4GalleryStyle,
	bs4AlphaGalleryStyle,
	bs5GalleryStyle,
	implicitFigures,
	implicitFiguresWith,
) where

import Text.Pandoc (
	Block (Div, Para, Plain),
	Inline (Image, SoftBreak),
	Pandoc)
import Text.Pandoc.Walk (walk)
import Data.Text (Text)

-- | Describes CSS classes to use by 'implicitFiguresWith'.
data GalleryStyle = GalleryStyle {
	galleryStyleFigureClass :: [Text],
	galleryStyleImageClass :: [Text],
	galleryStyleImageWithCaptionClass :: [Text],
	galleryStyleCaptionBlockClass :: [Text],
	galleryStyleCaptionTextClass :: [Text]
}

-- | Bootstrap 4 classes to use for 'implicitFiguresWith'.
bs4GalleryStyle :: GalleryStyle
bs4GalleryStyle = GalleryStyle {
	galleryStyleFigureClass = ["card", "card-sm"],
	galleryStyleImageClass = ["card-img"],
	galleryStyleImageWithCaptionClass = ["card-img-top"],
	galleryStyleCaptionBlockClass = ["card-body"],
	galleryStyleCaptionTextClass = ["card-text"]
}

-- | Bootstrap 4 Alpha classes to use for 'implicitFiguresWith'.
-- https://stackoverflow.com/questions/53750938/what-is-the-bootstrap-card-block-class
bs4AlphaGalleryStyle :: GalleryStyle
bs4AlphaGalleryStyle = bs4GalleryStyle {
	galleryStyleCaptionBlockClass = ["card-block"]
}

-- | Bootstrap 5 classes to use for 'implicitFiguresWith'.
bs5GalleryStyle :: GalleryStyle
bs5GalleryStyle = bs4GalleryStyle

-- | Replacement for 'Ext_implicit_figures' since it is broken by images wrapped in links by 'Hroch.Features.Gallery.makeThumbnails'.
--
-- This variant uses Bootstrap 4 Alpha styles.
implicitFigures :: Pandoc -> Pandoc
implicitFigures = implicitFiguresWith bs4AlphaGalleryStyle

-- | Replacement for 'Ext_implicit_figures' since it is broken by images wrapped in links by 'Hroch.Features.Gallery.makeThumbnails'.
--
-- It replaces an image wrapped in a paragraph by the following structure:
--
-- @
-- div.${figureClass}
-- ├─ img.${figureClass} or img.${imageWithCaptionClass}
-- └─ div.${captionBlockClass} (when caption present)
--   └─ div.${captionTextClass}
-- @
implicitFiguresWith :: GalleryStyle -> Pandoc -> Pandoc
implicitFiguresWith style = walk replaceFigures
	where
		replaceFigures :: Block -> Block
		replaceFigures (Para [Image (imgId, imgClasses, imgAttrs) content imgTarget]) =
				let
					caption = if null content then [] else [Div ("", galleryStyleCaptionBlockClass style, []) [Div ("", galleryStyleCaptionTextClass style, []) [Plain content]]]
				in
					Div
						("", galleryStyleFigureClass style, [])
						([
							Plain [SoftBreak, Image (imgId, (if null content then galleryStyleImageClass style else galleryStyleImageWithCaptionClass style) ++ imgClasses, imgAttrs) content imgTarget, SoftBreak]
						] ++ caption)
		replaceFigures o = o
