-- SPDX-License-Identifier: MIT
-- SPDX-FileCopyrightText: 2023 Jan Tojnar <jtojnar@gmail.com>

{-|
= Menu field

This is a field for Hakyll templates that will pick up the compiled contents of the @\@menu.md@ file that lies closest to the page’s file in the directory tree.

== Usage

Define the field in the rules like this:

@
{-# LANGUAGE OverloadedStrings #-}
import Hakyll
import Hroch.Features.Menu (menuField)
import Hroch.Internal.Utils (runHakyllWithHelpFlagForTests)

matchMenu :: Rules ()
matchMenu = do
	match (fromRegex "^pages/(.+/)?@menu\\.md$") $ do
		compile pandocCompiler

matchPage :: Rules ()
matchPage = do
	match (fromRegex "^pages/(.+/)?[^@/][^/]+\\.md$") $ do
		route $ stripPages `composeRoutes` setExtension "html"
		let ctx = menuField <> defaultContext
		compile $ pandocCompiler
			>>= loadAndApplyTemplate "templates/layout.html" ctx

stripPages :: Routes
stripPages = gsubRoute "pages/" (const "")

main :: IO ()
main = runHakyllWithHelpFlagForTests $ hakyll $ do
	matchMenu
	matchPage
@

Then, you should be able to use @$menu$@ variable in the layout.
-}
module Hroch.Features.Menu (menuField) where

import Control.Monad.Except (catchError, throwError)
import Hakyll
import System.FilePath (takeDirectory, (</>))

-- | Creates a field containing the contents of the closest menu file.
menuField :: Context String
menuField = field "menu" $ \_ -> do
	path <- toFilePath <$> getUnderlying
	menu <- loadMenu path
	return $ itemBody menu

loadMenu :: FilePath -> Compiler (Item String)
loadMenu path =
	let
		menuPath = fromFilePath $ takeDirectory path </> "@menu.md"
	in
		catchError
			(load menuPath)
			(\err ->
				if path == "." then
					throwError err
				else
					loadMenu (takeDirectory path)
			)
