-- SPDX-License-Identifier: MIT
-- SPDX-FileCopyrightText: 2023 Jan Tojnar <jtojnar@gmail.com>

{-# LANGUAGE ViewPatterns #-}

{-|
Stability: experimental

This is a set of internal functions.

API can change without bumping semver.
-}
module Hroch.Internal.Utils (
	both,
	getUriPath,
	isUriRelative,
	lowercase,
	listItems,
	normalisePath,
	optional,
	runHakyllWithHelpFlagForTests,
) where

import Data.Char (toLower)
import Data.List (intercalate)
import Data.Maybe (isJust)
import Data.Text (unpack)
import qualified Data.List.NonEmpty as NonEmpty
import System.Environment (withArgs)
import System.FilePath (joinPath, normalise, splitDirectories)
import Text.URI (URI)
import qualified Text.URI as URI

-- | Apply a single function to both components of a pair.
both :: (a -> b) -> (a, a) -> (b, b)
both f (a, b) = (f a, f b)

-- | Check whether a URL is relative.
--
-- >>> isUriRelative <$> URI.mkURI "http://example.org/foo"
-- False
--
-- >>> isUriRelative <$> URI.mkURI "https:/foo"
-- False
--
-- >>> isUriRelative <$> URI.mkURI "/foo/bar"
-- False
--
-- >>> isUriRelative <$> URI.mkURI "foo/bar"
-- True
--
-- >>> isUriRelative <$> URI.mkURI "mailto:adam@example.org"
-- False
isUriRelative :: URI -> Bool
isUriRelative uri = not (URI.isPathAbsolute uri || isJust (URI.uriScheme uri))

{-|
Gets path segment from URI as FilePath.

>>> getUriPath <$> URI.mkURI "foo.md"
"foo.md"

>>> getUriPath <$> URI.mkURI "/foo/bar.php?q=hello#test"
"/foo/bar.php"

>>> getUriPath <$> URI.mkURI "https://example.com:443/foo.html#test"
"/foo.html"
-}
getUriPath :: URI -> FilePath
getUriPath uri = intercalate "/" (optional (URI.isPathAbsolute uri) [] <> path)
	where
		path =
			case URI.uriPath uri of
				Nothing -> []
				Just (hasTrailing, NonEmpty.toList ->pathPieces) -> map (unpack . URI.unRText) pathPieces <> optional hasTrailing "/"


-- | Convert a string to lowercase.
lowercase :: String -> String
lowercase = map toLower

-- | Returns a singleton containing given @value@ when the @condition@ is @True@ or an empty “container” when it is @False@.
--
-- >>> optional True 42 :: [Int]
-- [42]
--
-- >>> optional False 42 :: [Int]
-- []
optional :: (Applicative f, Monoid (f a)) => Bool -> a -> f a
optional condition value = if condition then pure value else mempty

-- | 'normalise' does not remove “..” because of symlinks.
-- We want cleaner URLs so let’s just assume there are no symlinks in the site content.
-- This will allow us to drop those path segments as well.
normalisePath :: FilePath -> FilePath
normalisePath = reconstitute . foldr handleSegment ([], 0) . splitDirectories . normalise
	where
		-- “normalise” will prune multiplicate directory separators.
		handleSegment ".." (path, up) = (path, up + 1)
		handleSegment seg (path, 0) = (seg : path, 0)
		handleSegment _seg (path, up) = (path, up - 1)

		reconstitute (segs, up) = joinPath (replicate up ".." ++ segs)

{-|
Pretty print list of items.

>>> listItems []
""

>>> listItems ["foo"]
"‘foo’"

>>> listItems ["foo", "bar"]
"‘foo’ and ‘bar’"

>>> listItems ["foo", "bar", "baz"]
"‘foo’, ‘bar’ and ‘baz’"

>>> listItems ["foo", "bar", "baz", "qux"]
"‘foo’, ‘bar’, ‘baz’ and ‘qux’"
-}
listItems :: [String] -> String
listItems [x] = "‘" <> x <> "’"
listItems [x, y] = listItems [x] <> " and " <> listItems [y]
listItems (x:xs) = listItems [x] <> ", " <> listItems xs
listItems [] = ""

-- | Can be used to wrap @hakyll@ function in doctests so that it is passed @--help@ flag.
-- Otherwise the @main@ would fail because test runner will not pass any subcommand.
runHakyllWithHelpFlagForTests :: IO () -> IO ()
runHakyllWithHelpFlagForTests = withArgs ["--help"]
